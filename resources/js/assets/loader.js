$(document).ready(function() {

	setTimeout(function(){
		$("#depreload .wrapper").animate({ opacity: 1 });
	}, 400);

	setTimeout(function(){
		$("#depreload .logo").animate({ opacity: 1 });
	}, 800);

	var canvas  = $("#depreload .line")[0],
	context = canvas.getContext("2d");

	context.beginPath();
	context.arc(180, 180, 160, Math.PI * 1.5, Math.PI * 1.6);
	context.strokeStyle = '#138A4E';
	context.lineWidth = 5;
	context.stroke();

	var loader = $("body").DEPreLoad({
		OnStep: function(percent) {
			//console.log(percent + '%');

			$(".process-val").text(percent + "%");

			$("#depreload .line").animate({ opacity: 1 });
			$("#depreload .perc").text(percent + "%");

			if (percent > 5) {
				context.clearRect(0, 0, canvas.width, canvas.height);
				context.beginPath();
				context.arc(180, 180, 160, Math.PI * 1.5, Math.PI * (1.5 + percent / 50), false);
				context.stroke();
			}
		},
		OnComplete: function() {
			//console.log('Everything loaded!');
			$("#depreload .perc").text("done");
			$("#depreload .circle").animate({ opacity: 0 }); 
			$(".openLeft").stop().delay(500).animate({ width: '0' });
			$(".openRight").stop().delay(500).animate({ width:'0' });
			$("#depreload").delay(1400).fadeOut(300);
		}
	});
});