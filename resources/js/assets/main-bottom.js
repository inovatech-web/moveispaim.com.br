/**
* Created with moveispaim.com.br.
* User: pedruino
* Date: 2016-06-02
* Time: 04:05 PM
* To change this template use Tools | Templates.
*/
$(document).ready(function() {
   
   //topmenu pin
   var topmenu = $(".pin-top");
   $(window).scroll(function () {
      if( $(this).scrollTop() > 60 ) {
         topmenu.addClass("pinned");
      } else {
         topmenu.removeClass("pinned");
      }
   });
   
   //animations
   new WOW().init();
});