/**
* Created with moveispaim.com.br.
* User: pedruino
* Date: 2016-06-12
* Time: 08:56 PM
* To change this template use Tools | Templates.
*/
$(document).ready(function() {
   $(".fancybox").fancybox({
      closeBtn: true,
      arrows: true,
      padding: 5,
      helpers: {
         thumbs: { width: 50, height: 50}, // requires to include thumbs js and css files
         media: {}, // requires to include media js file
         buttons: {}, // requires to include buttons js and css files
         overlay : {
            css : { 'overflow-y' : 'hidden' }
         }
      },
      
      tpl:{
         closeBtn:'<a title="Fechar" class="fancybox-item fancybox-close"></a>',
         next:'<a title="Próxima" class="fancybox-nav fancybox-next"><span></span></a>',
         prev:'<a title="Anterior" class="fancybox-nav fancybox-prev"><span></span></a>'}
   });
});