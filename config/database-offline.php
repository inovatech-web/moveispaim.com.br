<?php
//$title = "Moveis Paim";
/**
 * Document   : src/data/OfflineDatabase.php
 * Created on : 2016-05-19 12:54 AM
 *
 * @author Pedro Escobar
 */
use Model\Phone;
use Model\Address;
use Model\SocialMedia;
use Model\Category;
use Model\Photo;

class DbContext {
   public $addresses;
   public $socialMedias;
   public $phones;
   public $categories;

   function __construct(){
      $this->addresses = $this->getAddresses();
      $this->socialMedias = $this->getSocialMedias();
      $this->phones = $this->getPhones();
      $this->categories = $this->getCategories();
   }

   private function getAddresses(){
      return array(
         new Address([
            'street' => 'Travessão Dom Pedro II, 7ª Légua, Capela Nossa Senhora das Dores',
            'number' => '',
            'zipcode' => '',
            'neighborhood' => '',
            'city' => 'Caxias do Sul',
            'state' => 'RS'
         ])
      );
   }

   private function getSocialMedias(){
      return array(
         new SocialMedia(['type' => 'facebook', 'url' => 'https://www.facebook.com/moveispaim', 'tooltip' => 'Like us on Facebook']),
         new SocialMedia(['type' => 'google-plus', 'url' => 'https://www.facebook.com/moveispaim', 'tooltip' => 'Follow us on Google Plus']),
      );
   }

   private function getPhones(){
      return array(
         new Phone(['number' => '(54) 9151-4759'])
      );
   }

   private function getCategories(){
      return array(
         [
            'Category' => new Category([ 'id' => '1', 'name' => 'bathrooms']),
            'Photo' => [
               new Photo(['src' => 'bathroom1.jpg', 'url' => '#!',]),
               new Photo(['src' => 'bathroom2.jpg', 'url' => '#!',]),
               new Photo(['src' => 'bathroom3.jpg', 'url' => '#!',]),
               new Photo(['src' => 'bathroom4.jpg', 'url' => '#!',]),
               new Photo(['src' => 'bathroom5.jpg', 'url' => '#!',]),
            ]
         ],    
         [
            'Category' => new Category([ 'id' => '2', 'name' => 'bedrooms']),
            'Photo' => [
               new Photo(['src' => 'bedroom1.jpg', 'url' => '#!' ]),
               new Photo(['src' => 'bedroom2.jpg', 'url' => '#!' ]),
               new Photo(['src' => 'bedroom3.jpg', 'url' => '#!' ]),
            ]
         ],   
         [
            'Category' => new Category([ 'id' => '3', 'name' => 'closets']),
            'Photo' => [
               new Photo(['src' => 'closet1.jpg', 'url' => '#!']),
               new Photo(['src' => 'closet2.jpg', 'url' => '#!']),
               new Photo(['src' => 'closet3.jpg', 'url' => '#!']),
               new Photo(['src' => 'closet4.jpg', 'url' => '#!']),
               new Photo(['src' => 'closet5.jpg', 'url' => '#!']),
            ]
         ],  
         [
            'Category' => new Category([ 'id' => '4', 'name' => 'corporate']),
            'Photo' => [
               new Photo(['src' => 'corporate1.jpg', 'url' => '#!']),
               new Photo(['src' => 'corporate2.jpg', 'url' => '#!']),
               new Photo(['src' => 'corporate3.jpg', 'url' => '#!']),
            ]
         ], 
         [
            'Category' => new Category([ 'id' => '5', 'name' => 'kitchens']),
            'Photo' => [
               new Photo(['src' => 'kitchen1.jpg', 'url' => '#!']),
               new Photo(['src' => 'kitchen2.jpg', 'url' => '#!']),
               new Photo(['src' => 'kitchen3.jpg', 'url' => '#!']),
               new Photo(['src' => 'kitchen4.jpg', 'url' => '#!']),
               new Photo(['src' => 'kitchen5.jpg', 'url' => '#!']),
            ]
         ], 
         [
            'Category' => new Category([ 'id' => '6', 'name' => 'livingrooms']),
            'Photo' => [
               new Photo(['src' => 'livingroom1.jpg', 'url' => '#!']),
               new Photo(['src' => 'livingroom2.jpg', 'url' => '#!']),
               new Photo(['src' => 'livingroom3.jpg', 'url' => '#!']),
               new Photo(['src' => 'livingroom4.jpg', 'url' => '#!']),
            ]
         ], 
      );
   }
}
/*
class Text {
   const HOME = "Bem-Vindo";
   const ABOUT = "A Empresa";
   const CONTACT = "Contato";
   const PROJECTS = "Projetos";
   const PRODUCTS = "Produtos";
   const NEWS = "Notícias";
   const LOCATION = "Como chegar";
   //projects
   const BATHROOMS = "Banheiros";
   const CLOSETS = "Closets";
   const CORPORATE = "Corporativo";
   const KITCHENS = "Cozinhas";
   const BEDROOMS = "Quartos";
   const LIVINGROOMS = "Sala de estar"; 

   const ADDRESS = "Endereço";
   const CONNECT = "Conecte";
   const PHONES = "Telefones";
   const REVENDA = "Revenda e Suporte";
   const HORARIO_ATENDIMENTO = "Horário de Atendimento";

   const SHOW_ALL = "Todos";
   const COPYRIGHT = "© Móveis Paim Ltda. Desenvolvido por <a class='teal-text text-lighten-3' href='http://inovatechinfo.com.br'>Inovatech Soluções Tecnológicas</a>";
}
*/
?>