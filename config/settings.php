<?php 
 /**
 * Document   : settings.php
 * Created on : 2016-06-03 01:11 AM
 *
 * @author Pedro Escobar
 */
date_default_timezone_set('America/Sao_Paulo');
/**
 * Use the DS to separate the directories in other defines
 */
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('ROOT')) {
	define('ROOT', dirname(__DIR__));
}


if (!defined('WWW_ROOT')) {
	define('WWW_ROOT', dirname(__DIR__) . DS);
}
if (!defined('SRC_DIR')) {
	define('SRC_DIR', ROOT.DS.'src'.DS);
}
if (!defined('RESOURCES_DIR')) {
	define('RESOURCES_DIR', ROOT.DS.'resources'.DS);
}

/**
 * Funcao que carrega as classes automaticamente
 * 
 */
function __autoload($clazz)
{
   $clazz = (DIRECTORY_SEPARATOR === '\\') ? str_replace('/', '\\', $clazz)  : str_replace('\\', '/', $clazz);
   $path = SRC_DIR."{$clazz}.php";
   //busca dentro da pasta classes a classe necessaria...
   include_once $path;
}
/**
 * Define charset
 * 
 * @UTF-8
 */
header('Content-type: text/html; charset=UTF-8');

/**
 * Config internationalization
 * 
 * @pt_BR
 */
$language = "pt_BR";
$domain = "messages";
$path_locale= WWW_ROOT."locale";

putenv("LANG=".$language);
setlocale(LC_ALL, $language);

bindtextdomain($domain, $path_locale);
textdomain($domain);

/**
 * Import essentials files
 * 
 * @database-offline
 * @HtmlHelper
 * @FormHelper
 */ 
require_once ('database-offline.php'); 
//require_once ('database.php');
require_once(SRC_DIR.'View/Helper/HtmlHelper.php'); 
require_once(SRC_DIR.'View/Helper/FormHelper.php'); 
