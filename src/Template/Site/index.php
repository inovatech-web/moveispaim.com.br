<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <?= $helper->Html->externalCss('//fonts.googleapis.com/icon?family=Material+Icons') ?>
      <?= $helper->Html->externalCss('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css') ?>
      <?= $helper->Html->css('materialize.min', ['media' => 'screen,projection']) ?>
      <?= $helper->Html->css('menu') ?>
      <?= $helper->Html->css('custom') ?>
      <?= $helper->Html->css('socialmedias', ['media' => 'screen,projection']) ?>      
      <?= $helper->Html->css('animate.min') ?>
      <?= $helper->Html->script('jquery-2.2.3.min') ?>
      <?= $helper->Html->script('modernizr-custom') ?>
      <title><?= _('title') ?></title>
   </head>
   <body>     
      <header>
         <?php element('splashscreen'); ?>
         <?php element('Layout/menu'); ?>
         <?php element('Layout/home'); ?>
      </header>
      <main class="col xs12 s12 m12 l12">                  	
         <section id="about" class="section scrollspy">
            <?php element('Section/about'); ?>
         </section>
         <section id="services" class="container section scrollspy">
            <?php //include('./src/View/projects/isotope.php'); ?>
         </section>
         <section id="location" class="section scrollspy">
            <?php element('Section/location'); ?>
         </section>
         <section id="contact" class="container section scrollspy">
            <?php element('Section/contact'); ?>
         </section>
         <footer class="page-footer black">   
            <?php element('Layout/footer'); ?>
         </footer>
      </main>      
      <?= $helper->Html->script('materialize.min') ?>
      <?= $helper->Html->externalScript('//maps.googleapis.com/maps/api/js?sensor=false') ?>
      <?= $helper->Html->script('assets/ca-main') ?>
      <?= $helper->Html->script('assets/maps') ?>
      <?= $helper->Html->script('wow.min') ?>
      <?= $helper->Html->script('jquery.DEPreLoad') ?>
      <?= $helper->Html->script('assets/loader') ?>
      <?= $helper->Html->script('assets/main-bottom') ?>
   </body>
</html>
