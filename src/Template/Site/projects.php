<?php 
require_once (dirname(dirname(dirname(__DIR__))).'/config/settings.php'); 
$db = new DbContext();

$helper = (object)[
   'Html' => new HtmlHelper(),
   'Form' => new FormHelper(),
];
?>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <?= $helper->Html->externalCss('//fonts.googleapis.com/icon?family=Material+Icons') ?>
      <?= $helper->Html->externalCss('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css') ?>
      <?= $helper->Html->css('materialize.min', ['media'=>'screen,projection']) ?>
      <?= $helper->Html->css('menu') ?>
      <?= $helper->Html->css('custom') ?>
      <?= $helper->Html->css('socialmedias', ['media'=>'screen,projection']) ?>      
      <?= $helper->Html->css('animate.min') ?>
      <?= $helper->Html->script('jquery-2.2.3.min') ?>
      <?= $helper->Html->script('modernizr-custom') ?>
      <!-- Add mousewheel plugin (this is optional) -->
      <?= $helper->Html->script('jquery.mousewheel-3.0.6.pack') ?>
      <!-- Add fancyBox main JS and CSS files -->
      <?= $helper->Html->script('fancybox/source/jquery.fancybox', ['version' => '2.1.5']) ?>
      <?= $helper->Html->css('fancybox/source/jquery.fancybox', ['version' => '2.1.5', 'media'=> "screen"]) ?>
      <!-- Add Thumbnail helper (this is optional) -->
      <?= $helper->Html->css('fancybox/source/helpers/jquery.fancybox-thumbs', ['version' => '1.0.7']) ?>
      <?= $helper->Html->script('fancybox/source/helpers/jquery.fancybox-thumbs', ['version' => '1.0.7']) ?>
      <style type="text/css">
         .caption-slider .middle-pos{
            border-left-color: #fff;
         }
         .caption-slider.topLevel h1{
            color: #fff;
         }
         .fancybox-margin{
            margin-right:0 !important;
         }
      </style>
      <title><?= _('title') ?></title>
   </head>
   <body>     
      <header>
         <?php element('splashscreen'); ?>
         <?php element('Layout/menu'); ?>
         <div id="pull-parallax" class="parallax-container scrollspy" style="height: 300px;">
            <span class="overlay"></span>
            <div class="parallax">
               <img class="" src="/img/bg-heading-projects.jpg" style="display: block; transform: translate3d(-50%, 286px, 0px);">
               <div class="caption-slider topLevel">
                  <div class="middle-pos">              
                     <h1><?= _('Projects') ?></h1>
                  </div>
               </div>
               <div class="caption-slider">
                  <div class="middle-pos">              
                     <h1><?= _('Projects') ?></h1>
                  </div>
               </div>
            </div>
            <span class="overlay-green"></span>
         </div>
         <svg id="bottomShape" width="100%" height="90px" fill="#f5f5f5" viewBox="0 0 1366 90" preserveAspectRatio="none">
            <path d="m 0.274329,73.862183 1366,0 L 1366,9.3475355 900.35901,39.96681 -0.00563127,0.20494098 z" stroke-width="0" stroke-dasharray="none" stroke-miterlimit="4"></path>
         </svg>
      </header>
      <main class="col xs12 s12 m12 l12">                  	
         <section class="container ca-blogs">      
            <article class="gallery-filter">
               <div class="button-group filters-button-group center-align">
                  <div class="hide-on-med-and-down show-on-large">
                     <button class="button waves-effect btn is-checked green accent-4" data-filter="*"><?= _('Show all') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-bathrooms"><?= _('Bathrooms') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-closets"><?= _('Closets') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-corporate"><?= _('Corporate') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-kitchens"><?= _('Kitchens') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-bedrooms"><?= _('Bedrooms') ?></button>
                     <button class="button waves-effect btn" data-filter=".filter-livingrooms"><?= _('Livingrooms') ?></button>
                  </div>
                  <div class="hide-on-large-only show-on-medium-and-down">
                     <!-- Dropdown Trigger -->
                     <a class="dropdown-button btn waves-effect" href="#" data-activates="dropdown-filter" data-beloworigin="true">Select Filter</a>
                     <ul id="dropdown-filter" class="dropdown-content">
                        <li>
                           <button class="button btn" data-filter="*"><?= _('Show all') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-bathrooms"><?= _('Bathrooms') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-closets"><?= _('Closets') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-corporate"><?= _('Corporate') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-kitchens"><?= _('Kitchens') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-bedrooms"><?= _('Bedrooms') ?></button>
                        </li>
                        <li>
                           <button class="button btn" data-filter=".filter-livingrooms"><?= _('Livingrooms') ?></button>
                        </li>
                     </ul>
                     <!-- Dropdown Structure -->
                  </div>
               </div>
               <ul class="grid js-isotope">
                  <?php foreach($db->categories as $category): ?>
                  <?php foreach($category['Photo'] as $photo):?>
                  <li class="grid-item filter-<?= $category['Category']->name ?>" data-category="filter-<?= $category['Category']->name ?>">
                     <a class="fancybox" rel="<?= $category['Category']->name ?>" href="/img/gallery/<?= $category['Category']->name ?>/<?= $photo->src ?>">
                        <img class="responsive-img" src="/img/gallery/<?= $category['Category']->name ?>/<?= $photo->src ?>">
                     </a>
                  </li>
                  <?php endforeach;?>
                  <?php endforeach;?>
               </ul>
            </article>
         </section>
         <footer class="page-footer black">   
            <?php element('Layout/footer'); ?>
         </footer>
      </main>      
      <?= $helper->Html->script('materialize.min') ?>
      <?= $helper->Html->script('assets/ca-main') ?>
      <?= $helper->Html->script('wow.min') ?>
      <?= $helper->Html->script('jquery.DEPreLoad') ?>
      <?= $helper->Html->script('assets/loader') ?>
      <?= $helper->Html->script('assets/main-bottom') ?>
      <?= $helper->Html->script('assets/fancybox') ?>      
   </body>
</html>
