<div id="pull-parallax" class="parallax-container scrollspy">
   <span class="overlay"></span>
   <div class="parallax">
      <img class="" src="/img/bg-parallax.jpg" style="display: block; transform: translate3d(-50%, 141px, 0px);">
      <div class="caption-slider topLevel">
         <div class="middle-pos">
            <span>Bem-vindo</span>
            <h1>Móveis Paim</h1>
         </div>
      </div>
      <div class="caption-slider">
         <div class="middle-pos">
            <span>Bem-vindo</span>
            <h1>Móveis Paim</h1>
         </div>
      </div>
   </div>
   <span class="overlay-green"></span>
</div>
<svg id="bottomShape" width="100%" height="90px" fill="#f5f5f5" viewBox="0 0 1366 90" preserveAspectRatio="none">
   <path d="m 0.274329,73.862183 1366,0 L 1366,9.3475355 900.35901,39.96681 -0.00563127,0.20494098 z" stroke-width="0" stroke-dasharray="none" stroke-miterlimit="4"></path>
</svg>