<?php global $db ?>

<div class="container">
    <div class="row">
        <div class="col l6 s12">
            <?php echo element('Box/facebook'); ?>
        </div>
        <div class="col l3 s12">
            <h5 class="white-text"><?= _('Phones') ?></h5>
            <ul>
                <?php foreach($db->phones as $phone):?>
                <li><a class="white-text waves-effect waves-light" href="#!"><i class="material-icons left">phone</i><?= $phone->number ?></a></li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="col l3 s12">
            <h5 class="white-text"><?= _('Connect') ?></h5>
            <span class="footer-social-icon">
               <?php foreach($db->socialMedias as $socialMedia):?>
                  <a href="<?= $socialMedia->url?>" class="icon_bar  icon_bar_<?= str_replace("-","_",$socialMedia->type)?> icon_bar_small" target="_blank">
                     <span class="t"><i class="fa fa-<?= $socialMedia->type ?>"></i></span>
                     <span class="b"><i class="fa fa-<?= $socialMedia->type ?>"></i></span>
                 </a>
               <?php endforeach; ?>
	         </span>
        </div>
    </div>
</div>
<div class="footer-copyright">
    <div class="container grey-text text-lighten-1">
        <?= sprintf(_('COPYRIGHT_MSG'), 'Móveis Paim', date('Y'), "<a class='teal-text text-lighten-3' href='http://inovatechinfo.com.br'>Inovatech Soluções Tecnológicas</a>") ?>
        <span class="right footer-social-icon">
			<a href="#home" class="waves-effect waves-light teal-text tooltipped" data-position="top" data-delay="50" data-tooltip="Voltar ao início"><i class="material-icons">navigation</i></a>
          <?php foreach($db->socialMedias as $socialMedia):?>
            <a data-position="top" data-delay="50" data-tooltip="<?= _($socialMedia->tooltip) ?>" class="grey-text text-lighten-1 tooltipped" href="<?= $socialMedia->url ?>">
              <i class="fa fa-<?= $socialMedia->type ?>"></i>    
           </a>
         <?php endforeach; ?>
		</span>
    </div>
</div>