
<?php 
global $helper;
$prefix = $_SERVER['REQUEST_URI'] == '/' ? '' : '/';
?>
<div class="navbar-inverse valign-wrapper nav-fixed">
   <nav role="navigation" class="pin-top">
      <div class="nav-wrapper container">
         <div class="ca-brand">
            <a href="index.html">
               <img src="/img/logo-paim.png" style="width: 150px;">
               <!--<div class="logo uppercase primaryColor valign">MóveisPaim <em></em></div>-->
            </a>
         </div>
         <ul class="top-menu hide-on-med-and-down right">
            <li><?= $helper->Html->link(_('Home'), '#pull-parallax', $prefix) ?></li>
            <li><?= $helper->Html->link(_('About'), '#about', $prefix) ?></li>
            <li><?= $helper->Html->link(_('Projects'), '/projetos') ?></li>
            <li><?= $helper->Html->link(_('Location'), '#location', $prefix) ?></li>
            <li><?= $helper->Html->link(_('Contact'), '#contact', $prefix) ?></li>
         </ul>      	
         <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
         <ul id="slide-out" class="">
            <li><?= $helper->Html->link(_('Home'), '#pull-parallax', $prefix) ?></li>
            <li><?= $helper->Html->link(_('About'), '#about', $prefix) ?></li>
            <li><?= $helper->Html->link(_('Projects'), '/projetos') ?></li>
            <li><?= $helper->Html->link(_('Location'), '#location', $prefix) ?></li>
            <li><?= $helper->Html->link(_('Contact'), '#contact', $prefix) ?></li>
         </ul>
      </div>
   </nav>
</div>