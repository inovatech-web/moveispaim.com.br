<article>
    <!-- Heading -->
    <div class="col xs12 s12 m12 l12">
        <div class="title-center">
            <div class="bottom-line">
                <h2><?= _('Location') ?></h2>
                <span class="line"><em></em></span>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- One -->
        <div class="col xs12 m12 s12 l12">
            <!--<iframe src="https://www.google.com/maps/d/embed?mid=1PVaDIQAdxrqfEw3ZTYBz-8lW0kc" width="100%" height="400"></iframe>-->
           <div id="map-canvas"></div>
        </div>
    </div>
</article>