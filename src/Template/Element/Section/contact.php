<article>
    <!-- Heading -->
    <div class="col xs12 m12 s12 l12">
        <div class="title-center wow bounceIn">
            <div class="bottom-line">
                <h2><?= _('Contact') ?></h2>
                <span class="line"><em></em></span>
            </div>
        </div>
        <div class="overhide">
            <div class="description col xs12 s12">
                <p class="center-align">
                    Entre em contato conosco pelo formulário abaixo ou pelo e-mail 
                   <a href="mailto:moveispaim@moveispaim.com.br" class="waves-effect">moveispaim@moveispaim.com.br</a>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- One -->
        <div class="col xs12 m6 s6 l6 wow fadeInLeft" data-wow-duration="1s">
            <?php echo element('Box/mail_form'); ?>
        </div>
        <!-- Second -->
        <div class="col xs12 m6 s6 l6 wow fadeInRight" data-wow-duration="1s">
            <?php echo element('Box/address'); ?>
            <?php //echo element('Box/open_hours'); ?>
        </div>
    </div>
</article>