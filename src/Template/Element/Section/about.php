<?php
	$images =
	[
		(object)['src' => './resources/img/about/Empresa-4.jpg'],
		(object)['src' => './resources/img/about/Empresa-7.jpg'],
		(object)['src' => './resources/img/about/Empresa-1.jpg'],
		(object)['src' => './resources/img/about/Empresa-6.jpg'],
		(object)['src' => './resources/img/about/Empresa-3.jpg'],
		(object)['src' => './resources/img/about/Empresa-2.jpg'],
		(object)['src' => './resources/img/about/Empresa-5.jpg'],
	];
?>
<article>
    <!-- Heading -->
    <div class="col xs12 m12 s12 l12">
        <div class="title-center">
            <div class="bottom-line">
                <h2><?= _('About') ?></h2>
                <span class="line"><em></em></span>
            </div>
        </div>
        <div class="overhide">
            <div class="description col xs12 s12">
                <p class="center-align">
                </p>
            </div>
        </div>
    </div>
    <div class="row">       
        <!-- One -->
        <div class="col xs12 m6 s6 l6 wow fadeInLeft">
            <div class="slider">
                <ul class="slides">
                    <?php foreach($images as $image):?>
                    <li>
                        <img src="<?php echo $image->src;?>" />
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <!-- Second -->
        <div class="col xs12 m6 s6 l6 wow fadeInRight">
            <div class="icon-block">
                <p>A Móveis Paim é uma empresa especializada na produção de móveis sob medida e no&nbsp;desenvolvimento de soluções exclusivas proporcionando conforto, beleza e bem-estar nas áreas residenciais e comerciais.</p>
                <p>Fundada em março de 1994 e radicada na Cidade de Caxias do Sul, tendo a qualidade como premissa norteadora, a Móveis Paim vem crescendo e conquistando espaço em meio a um dos principais pólos moveleiros do Brasil, o da Serra Gaúcha.</p>
                <p>Sem restrições para confecção de produtos, a Móveis Paim tem sensibilidade e versatilidade para planejar e personalizar seu ambiente com ampla variedade de cores e materiais. Sempre com responsabilidade ecológica, usando matéria prima 100% MDF e garantindo um futuro mais verde para você e sua família.</p>
                <p>Todos os produtos da Móveis Paim são desenvolvidos por uma equipe própria de design, marcenaria e montagem. Profissionais altamente qualificados, com experiência para criar novidades, suprindo as necessidades de cada ambiente. As etapas do processo de produção, desde a criação e simulação do projeto em perspectiva 3D até a montagem final e entrega dos móveis são acompanhadas e estudadas para que o resultado seja sempre o melhor.</p>
                <p>A qualidade e agilidade na produção de móveis sob medida e acessórios para cozinhas, dormitórios, closets, home theaters, home offices, áreas de serviço, banheiros, lojas e escritórios é compromisso da equipe e vem respaldando o crescimento da empresa.</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>    
</article>