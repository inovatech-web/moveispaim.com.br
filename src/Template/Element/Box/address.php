<?php global $db; ?>
<div class="icon-block">
   <h5 class="center"><?= _('Address') ?></h5>   
   <div class="center">
      <?php foreach($db->addresses as $address): ?>
      <p class="description">
         <?= $address->street.', '.$address->number ?><br>
         <?= $address->neighborhood.' / '.$address->zipcode ?><br>
         <?= $address->city.' - '.$address->state ?><br>
      </p>
      <?php endforeach; ?>
   </div>   
</div>