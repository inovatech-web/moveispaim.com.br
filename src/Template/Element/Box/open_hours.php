<div class="icon-block">
    <h5 class="center"><?= _('Open hours') ?></h5>

    <div class="center">
        <div class="bottom-line">
            <h6>Segunda à Sexta</h6>
            <span class="line"></span>
        </div>
        <h4 class="teal-text text-darken-2">8:00 às 12:00</h4>
        <h4 class="teal-text text-darken-2">13:30 às 18:00</h4>
    </div>
</div>