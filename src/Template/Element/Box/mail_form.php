<?php global $helper; ?>
<form action="!#">
   <div class="icon-block">
      <div class="row">
         <div class="input-field col s6">
            <?php echo $helper->Form->input('Nome', 'name');?>
         </div>
         <div class="input-field col s6">
            <?php echo $helper->Form->input('E-mail', 'email', 'email');?>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <?php echo $helper->Form->input('Telefone', 'phone', 'tel');?>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <?php echo $helper->Form->textarea('Mensagem', 'message', ['class'=>'materialize-textarea']);?>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <button type="button" class="btn btn-large uppercase bold-600 col s12 waves-effect">Enviar</button>
         </div>
      </div>
   </div>
</form>