<?php require_once ('../../../config/settings.php');
$error_code = http_response_code();

$helper = (object)[
   'Html' => new HtmlHelper(),
   'Form' => new FormHelper(),
];
?>
<!DOCTYPE html>
<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <?= $helper->Html->externalCss('//fonts.googleapis.com/icon?family=Material+Icons') ?>
      <?= $helper->Html->externalCss('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css') ?>
      <?= $helper->Html->css('materialize.min', ['media' => 'screen,projection']) ?>
      <?= $helper->Html->script('modernizr-custom') ?>
      <?= $helper->Html->script('jquery-2.2.3.min') ?>
   </head>
   <body> 
      <main>
         <?php include_once "{$error_code}.php"; ?>
      </main>
      <?= $helper->Html->script('materialize.min') ?>
   </body>
</html>