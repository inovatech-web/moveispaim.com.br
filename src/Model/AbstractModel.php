<?php
namespace Model;
/**
 * Document   : src/Model/Model.php
 * Created on : 2016-06-03 12:17 AM
 *
 * @author Pedro Escobar
 */
   
class AbstractModel{
   function __construct($properties) {		
      foreach($properties as $name => $value){
         $this->__set($name, $value);
      }
   }	

   public function __set($name, $value) {
      $this->$name = $value;
   }

}