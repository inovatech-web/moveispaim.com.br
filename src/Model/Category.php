<?php
namespace Model;
use Model\AbstractModel;

 /**
 * Document   : src/Model/Category.php
 * Created on : 2016-06-03 01:37 PM
 *
 * @author Pedro Escobar
 */ 
class Category extends AbstractModel {
    public $id;
    public $name;
}
