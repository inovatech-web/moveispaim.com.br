<?php
/**
 * Document   : src/Model/Phone.php
 * Created on : 2016-06-02 07:23 PM
 *
 * @author Pedro Escobar
 */
namespace Model;
use Model\AbstractModel;

class Phone extends AbstractModel{
   public $number;
}