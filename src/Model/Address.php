<?php
/**
 * Document   : src/Model/Phone.php
 * Created on : 2016-06-02 07:23 PM
 *
 * @author Pedro Escobar
 */
namespace Model;
use Model\AbstractModel;

class Address extends AbstractModel{
   public $street;
   public $number;
   public $zipcode;
   public $neighborhood;
   public $city;
   public $state;
}