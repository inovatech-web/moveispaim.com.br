<?php
namespace Model;
use Model\AbstractModel;

 /**
 * Document   : src/Model/Photo.php
 * Created on : 2016-06-03 01:46 PM
 *
 * @author Pedro Escobar
 */ 
class Photo extends AbstractModel {
    public $src;
    public $url;
}
