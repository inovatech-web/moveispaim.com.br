<?php
namespace Model;
use Model\AbstractModel;

 /**
 * Document   : src/Model/SocialMedia.php
 * Created on : 2016-06-03 01:44 AM
 *
 * @author Pedro Escobar
 */ 
class SocialMedia extends AbstractModel {
   public $type;
   public $url;
   public $tooltip;
}
