<?php
/**
 * Document   : src/view/Helper/HtmlHelper.php
 * Created on : 2016-05-11 10:58 PM
 *
 * @author Pedro Escobar
 */

class HtmlHelper{

   function link($text, $url, $prefix="", $target=""){
      echo '<a href="'.$prefix.$url.'" target='.$target.'>'.$text.'</a>';
   }

   function css($filename, $options = array()){
      $options = array_merge(array(         
         'path' => '/css/', 
         'media' => null,
         'version' => null
      ), $options); 
      
      echo '<link type="text/css" rel="stylesheet" href="'.$options['path'].$filename.'.css'.$this->get_version($options['version']).'"'.$this->get_media($options['media']).'/>';
   }

   function externalCss($filename, $media=null){
      echo '<link type="text/css" rel="stylesheet" href="'.$filename.'"'.$this->get_media($media).'/>';
   }
   function script($filename, $options = array()){
      $options = array_merge(array(
         'path' => '/js/', 
         'version' => null
      ), $options); 

      echo '<script type="text/javascript" src="'.$options['path'].$filename.'.js'.$this->get_version($options['version']).'"></script>';
   }

   function externalScript($filename){
      echo '<script type="text/javascript" src="'.$filename.'"></script>';
   } 
   
   private function get_media($media){
      if(isset($media))
         $media = ' media="'.$media.'"';   
      return $media;
   }

   private function get_version($version){
      if(isset($version))
         $version = "?v=".$version;   
      return $version;
   }
}



function element($filename=null){    
   if($filename){
      //var_dump($this->get_defined_vars());
      //global $data;      
      include(SRC_DIR.DS.'Template/Element/'.$filename.'.php');
   }      
}