<?php
/**
 * Document   : src/view/Helper/FormHelper.php
 * Created on : 2016-06-02 03:41 AM
 *
 * @author Pedro Escobar
 */

class FormHelper{
   
   function input($label, $id, $type='text', $options=['class'=>'validate']){
      $classes = $options['class'];
      $html  = '<input id="'.$id.'" type="'.$type.'" class="'.$classes.'">';
      $html .= '<label for="'.$id.'">'.$label.'</label>';

      return $html;
   }

   function textarea($label, $id, $options=['class'=>'']){
      $classes = $options['class'];
      $html  = '<textarea id="'.$id.'" type="textarea" class="'.$classes.'"></textarea>';
      $html .= '<label for="'.$id.'">'.$label.'</label>';

      return $html;
   }
}